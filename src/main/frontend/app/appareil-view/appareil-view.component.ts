import { Component, OnInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit {

  isAuth:boolean = false;
  appareils:any[];
  oneAppareilName:String = 'Lave Linge';
  secondAppareilName:String = 'Lave Vaisselle';
  lastUpdate= new Promise((resolve,reject)=>{
    const date=new Date();
    setTimeout(
      ()=>resolve(date),4000
    );
  });
  thirdAppareilName:String = 'Sèche Linge';
  test:any;
  appareilSubscription: Subscription;
  constructor(private appareilService:AppareilService){
    setTimeout(()=>this.isAuth=true,4000);
  }
  ngOnInit(){
  
    this.appareilSubscription = this.appareilService.appareilsSubject.subscribe(

      (appareils: any[]) => {

        this.appareils = appareils;

      }

    );
  }
  onAllumer()
  {
    console.log("Afficher");
    this.appareilService.switchOnAll();
  }
  onEteindre(){
    if(confirm('Etes-vous sûr de vouloir éteindre tous vos appareils ?')) {

      this.appareilService.switchOffAll();

    } else {

      return null;

    }
    console.log("viewtest2",this.test);
  }
  etat()
  {
    if(this.isAuth)
    {
      return 'allumé';
    }
    else
    {
      return 'éteint'
    }
  }
  onSave() {

    this.appareilService.saveAppareilsToServer();

}
onFetch() {

  this.appareilService.getAppareilsFromServer();

}
}
