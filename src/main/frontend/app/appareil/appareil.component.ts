import { Component, OnInit,Input,AfterViewInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service';
import { Observable } from 'rxjs/Observable';
import { fromEvent } from 'rxjs';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/scan';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit,AfterViewInit {

  @Input() appareilName:string;
  @Input() appareilStatus: String;
  @Input() index:number;
  @Input() id: number;
   ok:any;
  constructor(private appareilService:AppareilService) { }
  ngAfterViewInit(){


///////////////////////////////////////////////////:
    //import { fromEvent } from 'rxjs';
     
    // const el = document.getElementById('my-element');
     
    // // Create an Observable that will publish mouse movements
    // const mouseMoves = fromEvent(el, 'mousemove');
     
    // // Subscribe to start listening for mouse-move events
    // const subscription = mouseMoves.subscribe((evt: MouseEvent) => {
    //   // Log coords of mouse movements
    //   console.log(`Coords: ${evt.clientX} X ${evt.clientY}`);
     
    //   // When the mouse is over the upper-left of the screen,
    //   // unsubscribe to stop listening for mouse movements
    //   if (evt.clientX < 40 && evt.clientY < 40) {
    //     subscription.unsubscribe();
    //   }
    // });

/////////////////////////////////////////////////:///


  //  const essai=fromEvent(document.getElementById('test'),'click')
  //  .map(()=>state => Object.assign({}, state, {count: state.count + 1}));

  // const essai=fromEvent(document.getElementById('test'),'click');
  //  var bouton= document.getElementById('test');

    
  //   console.log("bouton",bouton);
  //   //const oups = essai.scan((state, changeFn) => changeFn(state), {count: 0});
    



  //   //oups.subscribe((valeur)=>this.ok=valeur);
    
  //   //essai.subscribe((evt: MouseEvent)=>console.log(evt.));

  //   console.log('ok',this.ok);  













  }
  ngOnInit(){ 
    const essai=fromEvent(document.getElementById('test'),'click')
    .map(()=>state => Object.assign({}, state, {count: state.count + 1}));
    
    const oups = essai.scan((state, changeFn) => changeFn(state), {count: 0});
     oups.subscribe((voir)=> this.test=voir)
     console.log("viewtest3",this.test);
  }

  getStatus(){
    return this.appareilStatus;
  }
  getColor() {

    if(this.appareilStatus === 'allumé') {

      return 'green';

    } else if(this.appareilStatus === 'éteint') {

      return 'red';

    }
  }
  onSwitch(){
    if(this.appareilStatus === 'allumé') {
      this.appareilService.switchOffOne(this.index);
    } else if(this.appareilStatus === 'éteint') {
      this.appareilService.switchOnOne(this.index);
    }
    console.log("viewtest3",this.test);
  }
  test(){

    
  }
}