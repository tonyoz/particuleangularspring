import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  isAuth: Boolean;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.isAuth = this.authService.isAuth;
  }
  onConnect() {
    this.authService.signIn().then(
      (etatConnexion) => {
        console.log('Sign in successful!');
        this.isAuth = Boolean.apply(etatConnexion);
        this.router.navigate(['appareils']);
      }
    )
  }
  onDisConnect() {
    this.authService.signOut().then(() => {
      this.isAuth = this.authService.isAuth;
      this.router.navigate(['api/login']);

    }).
      catch(() => {
        this.isAuth = this.authService.isAuth;
        this.router.navigate(['api/login']);

      });
  }
}
