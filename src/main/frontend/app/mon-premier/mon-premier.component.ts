import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mon-premier',
  templateUrl: './mon-premier.component.html',
  styleUrls: ['./mon-premier.component.scss']
})
export class MonPremierComponent implements OnInit {
  appareilName: String = 'Machine à Laver';
  appareilStatus:String ='teint';

  constructor() { }

  ngOnInit() {
  }

}
