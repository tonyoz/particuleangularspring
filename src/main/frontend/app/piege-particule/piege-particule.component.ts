import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/interval';
import { Particule } from '../models/particule.model';
import { Position } from '../models/position.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-piege-particule',
  templateUrl: './piege-particule.component.html',
  styleUrls: ['./piege-particule.component.scss']
})
export class PiegeParticuleComponent implements OnInit {


particule:Particule=new Particule(new Position(),new Position());
position1:Position=this.particule.position;
impulsion1:Position=this.particule.impulsion;

particule2:Particule=new Particule(new Position(),new Position());
nombreParticule:number=2;
particules:Particule[]=[];


  constructor(private router:Router) {
    this.particules=this.getParticules(this.nombreParticule);
    this.test(this.nombreParticule
    );

  }

  ngOnInit() { 
    const counter = Observable.interval(1000);
  }

 getParticules(nombreParticule:number):Particule[]
 {
   console.log(this.particules);
   for (let index = 0; index < nombreParticule; index++) {
    console.log("part",this.particules);
     this.particules.push(new Particule(new Position(),new Position()))     
   }
   return this.particules;
 }
  getParticule():void {
   console.log("dfs");
 }
//  *[Symbol.iterator](){
//   for(let i=0, l=this.nombreParticule; i < l; i++){
//      yield this.particules[i];
//   }
// }
test(nbParticule:number){
  let a=3;
  console.log('nbParticule',nbParticule);
   this.particules=new Array<number>(nbParticule as number).fill(1).map((value)=>new Particule(new Position(),new Position()));

 


} 
onSubmit(form:NgForm)
{

  this.nombreParticule=form.value['nombreParticule'];
  console.log("nb",this.nombreParticule);
  this.test(this.nombreParticule);
  this.router.navigate(['/appareils']);
}
}
