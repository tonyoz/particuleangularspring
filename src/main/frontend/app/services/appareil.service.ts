import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Particule } from '../models/particule.model';
import { Position } from '../models/position.model';


@Injectable()
export class AppareilService {
  thirdAppareilName: String = 'Sèche Linge';
  oneAppareilName: String = 'Lave Linge';
  secondAppareilName: String = 'Lave Vaisselle';


  appareilsSubject = new Subject<any[]>();


  private appareils = [

    {

      id: 1,

      name: 'Machine à laver',

      status: 'éteint'

    },

    {

      id: 2,

      name: 'Frigo',

      status: 'allumé'

    },

    {

      id: 3,

      name: 'Ordinateur',

      status: 'éteint'

    }

  ];

  constructor(private httpClient: HttpClient) {
  }

  saveAppareilsToServer() {
  this.httpClient.
  post('api/hello',
    {},
    {
      withCredentials: true 
    }

    ).subscribe(
      (response) => {

      // this.appareils = response;

        console.log("recup", response)

        this.emitAppareilSubject();

      },

      (error) => {

        console.log('Erreur ! : ' + error);
      }
    );

  }


  getAppareilsFromServer() {

    this.httpClient
      .get<any[]>('https://angularprojet-c7326.firebaseio.com/appareils.json')

      .subscribe(

        (response) => {

          this.appareils = response;

          console.log("recup", this.appareils)

          this.emitAppareilSubject();

        },

        (error) => {

          console.log('Erreur ! : ' + error);

        }

      );

  }
  switchOnAll() {
    for (let appareil of this.appareils) {
      appareil.status = 'allumé';

    }
    this.emitAppareilSubject();
  }

  switchOffAll() {
    for (let appareil of this.appareils) {
      appareil.status = 'éteint';
      this.emitAppareilSubject();
    }

  }
  switchOnOne(i: number) {

    this.appareils[i].status = 'allumé';
    this.emitAppareilSubject();

  }


  switchOffOne(i: number) {

    this.appareils[i].status = 'éteint';
    this.emitAppareilSubject();
  }

  getAppareilById(id: number) {
    const appareil = this.appareils.find(

      (s) => {
        return s.id === id;
      }
    )
    return appareil;
  }
  emitAppareilSubject() {
    this.appareilsSubject.next(this.appareils.slice());
  }

  addAppareil(name: string, status: string) {
    const appareilObject = {

      id: 0,

      name: '',

      status: ''

    };
    appareilObject.name = name;

    appareilObject.status = status;
    console.log('lengthappareo', this.appareils);
    appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;
    console.log("lengthmoin1", this.appareils.length - 1);
    console.log("bizarre", this.appareils[(this.appareils.length - 1)].id)
    console.log('uno', this.appareils);
    this.appareils.push(appareilObject);

    this.emitAppareilSubject();
  }
}