import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthService } from './auth.service';

import { Injectable } from '@angular/core';


@Injectable()

export class AuthGuard implements CanActivate {


  constructor(private authService: AuthService,

              private router: Router) { }

              route2:ActivatedRoute;
  canActivate(
   
    route: ActivatedRouteSnapshot,

    state: RouterStateSnapshot){

    if(this.authService.isAuth) {

      return true;

    } else {
      
      this.router.navigate(['/auth']);
    }

  }

}