import { HttpClient, HttpParams, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { resolve } from "q";
import { Resolve } from "@angular/router";

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {

  }
  isAuth = false;


   signIn() {
    const body: HttpParams = new HttpParams().
      set("password", "ADMIN").
      set("username", "ADMIN");



      // set("enabled", "false").set("accountNonExpired", 'false').
      // set("accountNonLocked", 'false').
      // set("accountNonLocked", 'false').
      // set("authorities", 'null');

    const header = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', contentType: 'string' });

    return new Promise((resolve, reject) =>
      this.http.
        post('api/login',
          body.toString(),
          {
            headers: header,
            observe: "response",
            responseType:"text",
            withCredentials: true 
          }
        ).subscribe
        (
        (respo:HttpResponse<null>) => {
         let HeResponseders = respo.headers;
          this.isAuth = true;
          resolve(this.isAuth);
          console.log('Sign in successful!');
            return (respo);
        },

        (error) => {
          this.isAuth = false;
          console.log('Erreur ! : ' + error);
          reject(this.isAuth)

        }
        )
    )
  }


  signOut() {
    const body: HttpParams = new HttpParams().
    set("password", "ADMIN").
    set("username", "ADMIN");



    // set("enabled", "false").set("accountNonExpired", 'false').
    // set("accountNonLocked", 'false').
    // set("accountNonLocked", 'false').
    // set("authorities", 'null');

  //const header = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', contentType: 'string' });

  return new Promise((resolve, reject) =>
    this.http.
      post('api/logout',
        body.toString(),
        {
         // headers: header,
          observe: "response",
          responseType:"text",
          withCredentials: true 
        }
      ).subscribe
      (
      (respo:HttpResponse<null>) => {
       let HeResponseders = respo.headers;
        this.isAuth = false;
        resolve(this.isAuth);
        console.log('Sign out successful!');
      },
      (error) => {
        this.isAuth = false;
        console.log('Erreur ! : ' + error);
        reject(this.isAuth)

      }
      )
  )
}
  }

