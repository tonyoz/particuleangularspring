package particule.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import particule.web.Model.Particule;
import particule.web.Model.Systeme;
import particule.web.Service.GestionParticule;
import particule.web.Service.GestionReactJava;
import particule.web.Service.GestionSysteme;

/**
 * Created by luhuiguo on 2017/4/13.
 */
@RestController
@RequestMapping("/api")
public class HelloResource {

  @Autowired
  GestionSysteme gestionSysteme;

  @Autowired
  GestionParticule gestionParticule;

  @Autowired
  GestionReactJava gestionReactJava;

  @PostMapping(path = "/hello", produces = "application/json")
  public Particule sayHello(@RequestBody Particule particule) {

    Systeme systemeParticule = new Systeme();
    Particule particuleAutre = new Particule(systemeParticule);
    gestionSysteme.recordSysteme(systemeParticule);
    systemeParticule.addParticule(particuleAutre);
    gestionParticule.recordParticule(particule);
//    gestionReactJava.PremierTest(1).subscribe(GestionReactJava::consumerTest);

    gestionReactJava.statistique(systemeParticule);

    return particule;
  }

  // @PostMapping("/login")
  // public ResponseEntity<User> login()
  // {
  // User user = (User)
  // SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  // return new ResponseEntity<User>(user, HttpStatus.OK);
  // }

}
