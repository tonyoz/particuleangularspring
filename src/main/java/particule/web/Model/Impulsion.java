package particule.web.Model;

import java.io.Serializable;
import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.ToString;

@Embeddable
@Getter
@ToString
class Impulsion implements Serializable {
 
    private Double x;
    private Double y;
    private Double z;

    public Impulsion(Double x, Double y, Double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Impulsion(IntToDoubleFunction fonction) {
        this.x = IntStream.of(50).mapToDouble(fonction).sum();
        this.y = IntStream.of(50).mapToDouble(fonction).sum();
        this.z = IntStream.of(50).mapToDouble(fonction).sum();
    }
    public Impulsion() {
        this.x =50.;
        this.y = 50.;
        this.z = 50.;
    }
}