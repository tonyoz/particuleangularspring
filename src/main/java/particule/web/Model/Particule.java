package particule.web.Model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.servlet.http.Part;

import io.reactivex.functions.Predicate;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import particule.web.Model.Utils.generationNombreAleatoire;


@Entity
@Getter
@ToString
public class Particule implements Serializable {
 
	 @ManyToOne
	 @Setter
	 private Systeme systeme;

	 private int systemeId;

	@Transient
	private Double maxDimX = 50.;
	@Transient
	private Double maxDimY = 50.;
	@Transient
	private Double maxDimZ = 50.;

	public Particule(Systeme sysParticule) {
		this.position=new Position(generationNombreAleatoire::genererDouble);
		this.spin=generationNombreAleatoire.generateSpin();
		this.impulsion=new Impulsion(generationNombreAleatoire::genererDouble);
		this.systemeId=sysParticule.getId();
		this.id=sysParticule.getNbParticule()+1;
	}
	public Particule() {
		this.position=new Position(generationNombreAleatoire::genererDouble);
		this.spin=generationNombreAleatoire.generateSpin();
		this.impulsion=new Impulsion(generationNombreAleatoire::genererDouble);
		this.systemeId=0;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	
	@AttributeOverrides({ @AttributeOverride(name = "x", column = @Column(name = "POSITION_X")),
			@AttributeOverride(name = "y", column = @Column(name = "POSITION_Y")),
			@AttributeOverride(name = "z", column = @Column(name = "POSITION_Z")) })
	private Position position;

	@AttributeOverrides({ @AttributeOverride(name = "x", column = @Column(name = "IMPULSION_X")),
			@AttributeOverride(name = "y", column = @Column(name = "IMPULSION_Y")),
			@AttributeOverride(name = "z", column = @Column(name = "IMPULSION_Z")) })
	private Impulsion impulsion;



	@Getter
	@Setter
	private Integer spin;

	public static Integer idParticule=100;
	public static Predicate<Particule> priseValeur()
	{return x->
		{if(  idParticule < x.getId())
			{
				if(x.getId()%idParticule==0)
			{idParticule+=100;
			return true;}
			else
			{
			idParticule+=1;
				return false;
			}
		}
		else
		{
			return false;
		}
	};
}

}