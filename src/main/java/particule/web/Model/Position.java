package particule.web.Model;

import java.io.Serializable;
import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Embeddable
class Position implements Serializable {
 
    private Double x;
    private Double y;
    private Double z;

    public Position(){}
    public Position(Double x, Double y, Double z) {
        this.x = x;
        this.y = y;
        this.z = z;

    }

    public Position(IntToDoubleFunction fonction) {
        this.x = IntStream.of(50).mapToDouble(fonction).sum();
        this.y = IntStream.of(50).mapToDouble(fonction).sum();
        this.z = IntStream.of(50).mapToDouble(fonction).sum();
    }

}