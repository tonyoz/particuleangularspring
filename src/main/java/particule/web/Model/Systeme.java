package particule.web.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import particule.web.Service.GestionSysteme;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Systeme implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int nbParticule;


    @OneToMany(mappedBy="systeme")
     private List<Particule> particulesList=new ArrayList<>();
        
    
    // @MapKeyColumn(name="particule_key")
    // private Map<Integer, Particule> particules=new HashMap<Integer,Particule>(){

    //     private static final long serialVersionUID = 1L;
    // };


    // @Transient
    // public List<Particule> particulesList() {
    //     return particules.values().stream().collect(Collectors.toList());
    // }

    // private void affectationSpins(IntSupplier fonction) {

    //     IntStream.range(0, particules.size()).mapToObj((i) -> {
    //         Particule particule = particules.get(i);
    //         particule.setSpin(fonction.getAsInt());
    //         return Pair.of(i, particule);
    //     }).collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));
    // }

    // public void config() {
    //     this.affectationSpins(generationNombreAleatoire::generateSpin);
    // }

     public Particule addParticule(Particule particule) {
       
    
       particulesList.add(particule);
       nbParticule = particulesList.size();

        // Map<Integer, Particule> newParticules = IntStream.range(0, nbParticulesAdd)
        //         .mapToObj((suit) -> Pair.of(nbParticuleSysteme + suit, new Particule()))
        //         .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));

        //particules.putAll(newParticules);
        

       
        return particule;
    }



}