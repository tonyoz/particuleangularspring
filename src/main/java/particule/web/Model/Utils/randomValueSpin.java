 package particule.web.Model.Utils;

import java.util.function.IntSupplier;

class randomValueSpin implements IntSupplier
{
    
    @Override
    public int getAsInt() {
        return -1 + (Math.random() * 2) > 0 ? 1 : -1;
    }
}