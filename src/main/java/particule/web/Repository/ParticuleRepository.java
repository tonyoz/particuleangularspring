package particule.web.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import particule.web.Model.Particule;

@Repository
public interface ParticuleRepository extends JpaRepository<Particule, Long> {
}