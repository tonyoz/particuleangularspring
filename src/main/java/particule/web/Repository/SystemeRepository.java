package particule.web.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import particule.web.Model.Systeme;

@Repository
public interface SystemeRepository extends JpaRepository<Systeme, Long> {
}