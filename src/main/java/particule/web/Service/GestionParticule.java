package particule.web.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import particule.web.Model.Particule;
import particule.web.Repository.ParticuleRepository;

@Service
public class GestionParticule{

   @Autowired 
   ParticuleRepository particuleRepository;

public Particule recordParticule(Particule particule)
{ 
    return particuleRepository.saveAndFlush(particule);
}

public List<Particule> recordListParticule(List<Particule> listParticule)
{
    return particuleRepository.save(listParticule);
}
}