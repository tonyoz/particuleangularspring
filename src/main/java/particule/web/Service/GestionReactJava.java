package particule.web.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import particule.web.Model.Particule;
import particule.web.Model.Systeme;
import particule.web.Model.Utils.takeStream;
import particule.web.Repository.ParticuleRepository;

@Service
public class GestionReactJava {

    ParticuleRepository particuleRepository;

    public Maybe<List<Particule>> PremierTest(int index) {

        return Observable.just(particuleRepository.findAll()).elementAt(index);

    }

    public static Integer idParticule = 5;

    static public void consumerTest(List<Particule> list) {
        list.stream().map((valeur) -> {
            valeur.setSpin(0);
            return valeur;
        }).close();
    }

    // Etude de la fonction random générant le spin
    /*
     * Génération de particules( Boucle) Evaluation du nombre de particules avec le
     * spin négatif(ou positif)(L'évaluation du nombre se fait toutes les 2
     * secondes) Calcul du pourcentage de particules possédant ce spin Arret de la
     * prise de mesure: l'écart entre les pourcentages calculés descend en dessous
     * de x +/- 0.05 par ex (49.95% < x < 50,05%) Cet écart doit être stablisé =>
     * (xPourcentage1-xPourcentage2)< 0.05 & (xPourcentage2-xPourcentage3)
     * equivalent à 4 secondes
     * 
     */

    public List<Double> statistique(Systeme systeme)

    {
        final long nombreParticuleSpin[] = { 0 };
        final double moyenneSpin[] = { 0 };
        final int numeroMoyenne[] = { 0 };
        //initialisation de la moyenne 
        List<Double> evolutionmoyenne = new ArrayList<>();
        evolutionmoyenne.add(0.);

        Stream<Pair<Integer, Particule>> deux = Stream.iterate(1, i -> i + 1).map(i -> systeme).map(system -> {
            return new Particule(system);
        }).peek(x -> {
            nombreParticuleSpin[0] = nombreParticuleSpin[0] + 1;
        }).limit(100).filter(x -> {
            if (idParticule <= x.getId()) {
                if (x.getId() % idParticule == 0) {
                    idParticule += idParticule;
                    return true;
                } else {
                    idParticule += 1;
                    return false;
                }
            } else {
                return true;
            }
        }).peek(x -> {
            numeroMoyenne[0] = x.getId() / idParticule;
            moyenneSpin[numeroMoyenne[0]] = (double) nombreParticuleSpin[0] / (x.getId());
            if(numeroMoyenne[0]==evolutionmoyenne.size()-1)
            {
            evolutionmoyenne.add(moyenneSpin[numeroMoyenne[0]]);
            }
        }).map(moyenne -> {
            return Pair.of(evolutionmoyenne.size() - 1, moyenne);
        });
        //Condition de sorti du flux: la différence de la moyenne actuelle avec celle calculé auparavent  divisé par la moyenne 
        //actuelle  doit être inférieur à 0.5%  
        Stream<Pair<Integer, Particule>> streamStatistics = takeStream.takeWhile(deux,
               x -> {
               return  Math.abs((evolutionmoyenne.get(x.getFirst()) - evolutionmoyenne.get(x.getFirst() - 1))
                        / evolutionmoyenne.get(x.getFirst())) < 0.005;});
        streamStatistics.toArray();

        return evolutionmoyenne;

    }

}
