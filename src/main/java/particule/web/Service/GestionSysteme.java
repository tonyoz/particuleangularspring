package particule.web.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import particule.web.Model.Particule;
import particule.web.Model.Systeme;
import particule.web.Repository.ParticuleRepository;
import particule.web.Repository.SystemeRepository;


@Service
public class GestionSysteme
{
    @Autowired
    SystemeRepository systemeRepository;

    @Autowired
    ParticuleRepository particuleRepository;    

    public Systeme recordSysteme(Systeme systeme)
    { 
        return systemeRepository.saveAndFlush(systeme);
    }

    
}